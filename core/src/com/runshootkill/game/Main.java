package com.runshootkill.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.runshootkill.game.screens.PreviewScreen;

/**
 * @author Лукьянов В. А.
 */
public class Main extends Game {
    public static SpriteBatch batch;
    public static int width;
    public static int height;

    @Override
    public void create() {
        batch = new SpriteBatch();
        width = Gdx.graphics.getWidth();
        height = Gdx.graphics.getHeight();

        setScreen(new PreviewScreen(this));
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();

        batch.dispose();
    }
}

package com.runshootkill.game.abstractСlasses;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.runshootkill.game.animation.Animation;

/**
 * Абстрактный класс актёров
 *
 * @author Лукьянов В. А.
 */
public abstract class Actor extends GraphicsObj {
    private Animation currentAnimation;
    private Animation tempCurrentAnimation;
    private Animation stepsR;
    private Animation stepsL;
    private Animation passStateR;
    private Animation passStateL;
    private Animation attackR;
    private Animation attackL;

    private float speed;
    private float damage;
    private float health;

    private Sound soundAttack;

    public Actor(Animation stepsR, Animation stepsL, Animation passStateR, Animation passStateL, Animation attackR, Animation attackL, Vector2 position, float speed, float health, float damage, Sound soundAttack, float width, float height) {
        this.stepsR = stepsR;
        this.stepsL = stepsL;
        this.passStateR = passStateR;
        this.passStateL = passStateL;
        this.attackR = attackR;
        this.attackL = attackL;
        this.speed = speed;
        this.health = health;
        this.damage = damage;
        this.soundAttack = soundAttack;

        setPosition(position);
        setBounds(new Rectangle(position.x - width / 2, position.y - height / 2, width, height));
        setWidth(width);
        setHeight(height);
        setDirection(new Vector2(0, 0));
    }

    public Animation getCurrentAnimation() {
        return currentAnimation;
    }

    public Animation getTempCurrentAnimation() {
        return tempCurrentAnimation;
    }

    public Animation getStepsR() {
        return stepsR;
    }

    public Animation getStepsL() {
        return stepsL;
    }

    public Animation getPassStateR() {
        return passStateR;
    }

    public Animation getPassStateL() {
        return passStateL;
    }

    public Animation getAttackR() {
        return attackR;
    }

    public Animation getAttackL() {
        return attackL;
    }

    public float getSpeed() {
        return speed;
    }

    public float getDamage() {
        return damage;
    }

    public float getHealth() {
        return health;
    }

    public Sound getSoundAttack() {
        return soundAttack;
    }

    public void setCurrentAnimation(Animation currentAnimation) {
        this.currentAnimation = currentAnimation;
    }

    public void setTempCurrentAnimation(Animation tempCurrentAnimation) {
        this.tempCurrentAnimation = tempCurrentAnimation;
    }

    public void setStepsR(Animation stepsR) {
        this.stepsR = stepsR;
    }

    public void setStepsL(Animation stepsL) {
        this.stepsL = stepsL;
    }

    public void setPassStateR(Animation passStateR) {
        this.passStateR = passStateR;
    }

    public void setPassStateL(Animation passStateL) {
        this.passStateL = passStateL;
    }

    public void setAttackR(Animation attackR) {
        this.attackR = attackR;
    }

    public void setAttackL(Animation attackL) {
        this.attackL = attackL;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }

    public void setHealth(float health) {
        this.health = health;
    }

    public void setSoundAttack(Sound soundAttack) {
        this.soundAttack = soundAttack;
    }

    public abstract void hit(float damage);
}

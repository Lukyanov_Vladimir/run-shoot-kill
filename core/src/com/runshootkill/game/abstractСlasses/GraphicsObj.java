package com.runshootkill.game.abstractСlasses;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.runshootkill.game.animation.Animation;

/**
 * Абстрактный класс графических объектов
 *
 * @author Лукьянов В. А.
 */
public abstract class GraphicsObj {
    private Texture imgObj;

    private Animation animationObj;

    private Rectangle bounds;

    private Vector2 position;
    private Vector2 direction;

    private float width;
    private float height;

    public GraphicsObj() {
    }

    public GraphicsObj(Texture imgObj, Vector2 position, float width, float height) {
        this.imgObj = imgObj;
        this.position = position;
        this.width = width;
        this.height = height;
    }

    public GraphicsObj(Animation animationObj, Vector2 position, float width, float height) {
        this.animationObj = animationObj;
        this.position = position;
        this.width = width;
        this.height = height;
    }

    public Texture getImgObj() {
        return imgObj;
    }

    public Animation getAnimationObj() {
        return animationObj;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public Vector2 getPosition() {
        return position;
    }

    public Vector2 getDirection() {
        return direction;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public void setImgObj(Texture imgObj) {
        this.imgObj = imgObj;
    }

    public void setAnimationObj(Animation animationObj) {
        this.animationObj = animationObj;
    }

    public void setBounds(Rectangle bounds) {
        this.bounds = bounds;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public void setDirection(Vector2 direction) {
        this.direction = direction;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public abstract void draw(SpriteBatch batch);

    public abstract void update(float dt);

    public abstract void dispose();
}

package com.runshootkill.game.animation;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

/**
 * Класс анимации, покадровой разбивки спрайтов
 *
 * @author Лукьянов В. А.
 */
public class Animation {
    private Array<TextureRegion> frames;
    private float maxFrameTime;
    private float currentFrameTime;
    private int frameCount;
    private int numFrame;

    /**
     * Конструктор класса Animation
     *
     * @param sprites - текстура с кадрами анимации
     * @param frameCount - кол-во кадров
     * @param cycleTime - время отображения 1 кадра
     */
    public Animation(Texture sprites, int frameCount, float cycleTime) {
        this.frameCount = frameCount;

        frames = new Array<>();

        int frameWidth = sprites.getWidth() / frameCount;

        for (int i = 0; i < frameCount; i++) {
            frames.add(new TextureRegion(sprites, i * frameWidth, 0, frameWidth, sprites.getHeight()));
        }

        maxFrameTime = cycleTime / frameCount;

        numFrame = 0;
    }

    /**
     * Метод обновляет отображаемый кадр
     *
     * @param dt - интервал времени между текущим и последним кадром в секундах
     */
    public void update(float dt) {
        currentFrameTime += dt;

        if (currentFrameTime > maxFrameTime) {
            numFrame++;
            currentFrameTime = 0f;
        }

        if (numFrame >= frameCount)
            numFrame = 0;
    }

    /**
     * Метод возвращает текущий кадр
     *
     * @return - текущий кадр
     */
    public TextureRegion getFrame() {
        return frames.get(numFrame);
    }

    /**
     * Метод возвращает номер теущкго кадра
     *
     * @return - номер текущего кадра
     */
    public int getNumFrame() {
        return numFrame;
    }

    /**
     * Метод возвращает ture, если анимация достигла последнего кадра
     *
     * @return - false или true
     */
    public boolean isEndAnimation() {
        return numFrame == frameCount - 1;
    }

    /**
     * Метод начинает анимацию сначала
     */
    public void returnToStart() {
        numFrame = 0;
    }
}

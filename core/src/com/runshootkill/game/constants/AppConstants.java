package com.runshootkill.game.constants;

public class AppConstants {
    public static final String APP_ICON = "appIcons\\app_icon.png";
    public static final String APP_TITLE = "Run shoot kill";
    public static final int APP_WIDTH = 800;
    public static final int APP_HEIGHT = 480;
}

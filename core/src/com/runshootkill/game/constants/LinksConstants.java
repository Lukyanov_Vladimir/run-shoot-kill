package com.runshootkill.game.constants;

public class LinksConstants {
    public static final String PREVIEW_IMG = "previews\\game_name.png";
    public static final String BG_MENU = "backgrounds\\bg_menu.png";
    public static final String GM_NAME = "backgrounds\\bg_logo.png";
    public static final String BTN_START_UNPR = "buttons\\btn_menu_start_unpress.png";
    public static final String BTN_START_PR = "buttons\\btn_menu_start_press.png";
    public static final String BTN_EXIT_UNPR = "buttons\\btn_menu_exit_unpress.png";
    public static final String BTN_EXIT_PR = "buttons\\btn_menu_exit_press.png";
    public static final String MAP = "maps\\map.png";
    public static final String JOYSTICK_CIRCLE = "joysticks\\joystick_bg.png";
    public static final String JOYSTICK_STICK = "joysticks\\joystick_stick.png";
    public static final String BTN_SHOOT = "buttons\\btn_game_shoot.png";
    public static final String P_BULLET_RIGHT = "bullets\\bulletRight.png";
    public static final String P_BULLET_LEFT = "bullets\\bulletLeft.png";
    public static final String P_STEPS_RIGHT = "playerAnimations\\p_steps_right.png";
    public static final String P_STEPS_LEFT = "playerAnimations\\p_steps_left.png";
    public static final String P_PASS_RIGHT = "playerAnimations\\p_pass_right.png";
    public static final String P_PASS_LEFT = "playerAnimations\\p_pass_left.png";
    public static final String P_SHOOT_RIGHT = "playerAnimations\\p_shoot_right.png";
    public static final String P_SHOOT_LEFT = "playerAnimations\\p_shoot_left.png";
    public static final String MEL_NPC_STEPS_RIGHT = "npcAnimations\\meleeNpc\\npc_steps_right.png";
    public static final String MEL_NPC_STEPS_LEFT = "npcAnimations\\meleeNpc\\npc_steps_left.png";
    public static final String MEL_NPC_PASS_RIGHT = "npcAnimations\\meleeNpc\\npc_dead_right.png";
    public static final String MEL_NPC_PASS_LEFT = "npcAnimations\\meleeNpc\\npc_dead_left.png";
    public static final String MEL_NPC_ATTACK_RIGHT = "npcAnimations\\meleeNpc\\npc_attack_right.png";
    public static final String MEL_NPC_ATTACK_LEFT = "npcAnimations\\meleeNpc\\npc_attack_left.png";
    public static final String SOUND_FIRE = "sound\\fire.mp3";
    public static final String SOUND_SWORD = "sound\\sword.mp3";
    public static final String BG_MUSIC = "music\\mainMenuScreen.mp3";
    public static final String INFO_PANEL = "infoPanel\\\\infoPanel.png";
    public static final String INFO_PANEL_HEARTS = "infoPanel\\\\infoPanel_hearts.png";
    public static final String INFO_PANEL_HEART = "infoPanel\\\\infoPanel_heart.png";
    public static final String INFO_PANEL_AMMO = "infoPanel\\\\infoPanel_ammo.png";
    public static final String INFO_PANEL_BULLET = "infoPanel\\\\infoPanel_bullet.png";
}
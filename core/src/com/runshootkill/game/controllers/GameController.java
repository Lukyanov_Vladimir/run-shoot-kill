package com.runshootkill.game.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.runshootkill.game.Main;
import com.runshootkill.game.entities.Player;
import com.runshootkill.game.universalInterfaces.Button;
import com.runshootkill.game.universalInterfaces.Joystick;

public class GameController implements InputProcessor {
    private Main main;
    private Button btnShoot;
    private Joystick joystick;
    private Player player;


    public GameController(Main main, Button btnShoot, Joystick joystick, Player palyer) {
        this.main = main;
        this.btnShoot = btnShoot;
        this.joystick = joystick;
        this.player = palyer;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        screenY = Main.height - screenY;
        update(new Vector2(screenX, screenY), true);
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        screenY = Main.height - screenY;
        update(new Vector2(screenX, screenY), false);
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        screenY = Main.height - screenY;
        update(new Vector2(screenX, screenY), true);
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public void update(Vector2 touch, boolean isDownTouch) {
        joystick.update(Gdx.graphics.getDeltaTime());
        joystick.update(touch, isDownTouch);
        player.setDirection(joystick.getDirection());
        player.attack(btnShoot.isPressed(touch, isDownTouch));
    }
}

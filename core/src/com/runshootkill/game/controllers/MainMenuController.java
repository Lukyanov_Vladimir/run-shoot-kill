package com.runshootkill.game.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.math.Vector2;
import com.runshootkill.game.Main;
import com.runshootkill.game.screens.GameScreen;
import com.runshootkill.game.universalInterfaces.Button;

public class MainMenuController implements InputProcessor {
    private Main main;
    private Button btnStart;
    private Button btnExit;
    private Music music;

    public MainMenuController(Main main, Button btnStart, Button btnExit, Music music) {
        this.main = main;
        this.btnStart = btnStart;
        this.btnExit = btnExit;
        this.music = music;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        screenY = Main.height - screenY;
        update(new Vector2(screenX, screenY), true);
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        screenY = Main.height - screenY;
        update(new Vector2(screenX, screenY), false);
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public void update(Vector2 touch, boolean isDownTouch) {
        if (btnStart.isPressed(touch, isDownTouch)) {
            main.setScreen(new GameScreen(main));
            music.stop();
        }

        if (btnExit.isPressed(touch, isDownTouch)) {
            Gdx.app.exit();
        }
    }
}

package com.runshootkill.game.entities;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.runshootkill.game.abstractСlasses.Actor;
import com.runshootkill.game.animation.Animation;
import com.runshootkill.game.utilities.Borders;

public class MeleeNPC extends Actor {

    private Player player;

    private boolean isDamage = false;

    public MeleeNPC(Player player, Animation stepsR, Animation stepsL, Animation passStateR, Animation passStateL, Animation attackR, Animation attackL, Vector2 position, float speed, float health, float damage, Sound soundAttack, float width, float height) {
        super(stepsR, stepsL, passStateR, passStateL, attackR, attackL, position, speed, health, damage, soundAttack, width, height);

        this.player = player;
    }

    @Override
    public void draw(SpriteBatch batch) {
        batch.draw(getCurrentAnimation().getFrame(), getBounds().x, getBounds().y, getWidth(), getHeight());
    }

    @Override
    public void update(float dt) {
        if (getCurrentAnimation() != getTempCurrentAnimation()) {
            returnToStartAnimations();
            setTempCurrentAnimation(getCurrentAnimation());
        }

        perimeterRestrictions();

        setAnimationSteps();

        attack();

        getCurrentAnimation().update(dt);

        getBounds().setPosition(new Vector2(getPosition().x - getWidth() / 2, getPosition().y - getHeight() / 2));
    }

    @Override
    public void dispose() {

    }

    /**
     * Метод устанвавливает анимацию шагов вправо
     */
    private void goRight() {
        setCurrentAnimation(getStepsR());
    }

    /**
     * Метод устанвавливает анимацию шагов влево
     */
    private void goLeft() {
        setCurrentAnimation(getStepsL());
    }

    /**
     * Метод устанвавливает правую пассивную анимацию
     */
    private void deadRight() {
        setCurrentAnimation(getPassStateR());
    }

    /**
     * Метод устанвавливает левую пассивную анимацию
     */
    private void deadLeft() {
        setCurrentAnimation(getPassStateL());
    }

    /**
     * Метод устанвавливает правую анимацию стрельбы
     */
    private void attackRight() {
        setCurrentAnimation(getAttackR());
    }

    /**
     * Метод устанвавливает левую анимацию стрельбы
     */
    private void attackLeft() {
        setCurrentAnimation(getAttackL());
    }

    private void setAnimationAttack() {
        if ((!getCurrentAnimation().equals(getAttackR())) && ((getCurrentAnimation().equals(getStepsR())) || (getCurrentAnimation().equals(getPassStateR())))) {
            attackRight();
        }

        if ((!getCurrentAnimation().equals(getAttackL())) && ((getCurrentAnimation().equals(getStepsL())) || (getCurrentAnimation().equals(getPassStateL())))) {
            attackLeft();
        }
    }

    private void attack() {
        if (getBounds().overlaps(player.getBounds())) {
            setAnimationAttack();
            if ((getCurrentAnimation().equals(getAttackR()) || getCurrentAnimation().equals(getAttackL())) && getCurrentAnimation().getNumFrame() == 1) {
                if (!isDamage) {
                    player.hit(getDamage());
                    getSoundAttack().play();
                    isDamage = true;
                }
            }

            if ((getCurrentAnimation().equals(getAttackR()) || getCurrentAnimation().equals(getAttackL())) && getCurrentAnimation().getNumFrame() != 1)
                isDamage = false;
        }
    }

    private void setAnimationSteps() {
        if (!getBounds().overlaps(player.getBounds())) {
            if (getDirection().x > 0) {
                goRight();
            }

            if (getDirection().x < 0) {
                goLeft();
            }
        }
    }

    private void returnToStartAnimations() {
        getStepsR().returnToStart();
        getStepsL().returnToStart();
        getPassStateR().returnToStart();
        getPassStateL().returnToStart();
        getAttackR().returnToStart();
        getAttackL().returnToStart();
    }

    private void control() {
        if (!getBounds().overlaps(player.getBounds())) {
            float dx = player.getPosition().x - getPosition().x;
            float dy = player.getPosition().y - getPosition().y;

            float dist = (float) Math.sqrt(dx * dx + dy * dy);

            if (dist != 0) {

                setDirection(new Vector2((dx / dist), (dy / dist)));

                getPosition().add(getDirection().x * getSpeed(), getDirection().y * getSpeed());
            }
        }
    }

    private void perimeterRestrictions() {
        System.out.println(getDirection().x+ " "+ getDirection().y);
        if (Borders.borderLeft.contains(getPosition().x - getWidth() / 2f, getPosition().y) && getDirection().x < 0) {
            getBounds().setPosition(getPosition().x - getWidth() / 2f, (getPosition().y - getHeight() / 2f));

        } else if (Borders.borderDown.contains(getPosition().x, getPosition().y - getHeight() / 2f) && getDirection().y < 0) {
            getBounds().setPosition(getPosition().x - getWidth() / 2f, (getPosition().y - getHeight() / 2f));

        } else if (Borders.borderRight.contains(getPosition().x + getWidth() / 2f - 45, getPosition().y) && getDirection().x > 0) {
            getBounds().setPosition(getPosition().x - getWidth() / 2f, (getPosition().y - getHeight() / 2f));

        } else if ((Borders.borderUpR.contains(getPosition().x, getPosition().y + getHeight() / 2f) && getDirection().y > 0) || ((Borders.borderUpR.contains(getPosition().x + getWidth() / 2f, getPosition().y)) &&  getDirection().x > 0)) {
            getBounds().setPosition(getPosition().x - getWidth() / 2f, (getPosition().y - getHeight() / 2f));

        }else if ((Borders.borderUpL.contains(getPosition().x, getPosition().y + getHeight() / 2f) && getDirection().y > 0) || ((Borders.borderUpL.contains(getPosition().x - getWidth() / 2f, getPosition().y)) &&  getDirection().x < 0)) {
            getBounds().setPosition(getPosition().x - getWidth() / 2f, (getPosition().y - getHeight() / 2f));

        } else {
            control();
        }
    }

    /**
     * Метод отвечает за нанесение урона
     *
     * @param damage - урон
     */
    @Override
    public void hit(float damage) {
        setHealth(getHealth() - damage);
    }
}

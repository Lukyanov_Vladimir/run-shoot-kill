package com.runshootkill.game.entities;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.runshootkill.game.abstractСlasses.Actor;
import com.runshootkill.game.animation.Animation;
import com.runshootkill.game.utilities.Borders;
import com.runshootkill.game.utilities.BulletGenerator;
import com.runshootkill.game.utilities.Resize;

/**
 * Класс создания игрока
 *
 * @author Лукьянов В. А.
 */
public class Player extends Actor {
    private boolean isAttackRight = false;
    private boolean isAttackLeft = false;
    private Resize resize;

    private Borders borders;

    private BulletGenerator bulletGenerator;

    /**
     * Конструктор класса Player
     *
     * @param stepsR      - анимация шагов вправо
     * @param stepsL      - анимация шагов влево
     * @param passStateR  - анимация пассивнного состояния (правая)
     * @param passStateL  - анимация пассивнного состояния (левая)
     * @param shootingR   - анимация стрельбы вправо
     * @param shootingL   - анимация стрельбы влево
     * @param position    - позиция
     * @param speed       - скорость
     * @param health      - жизни
     * @param damage      - урон
     * @param soundAttack - звук атаки
     * @param width       - ширина
     * @param height      - высота
     */
    public Player(Animation stepsR, Animation stepsL, Animation passStateR, Animation passStateL, Animation shootingR, Animation shootingL, Texture bulletRight, Texture bulletLeft, Vector2 position, float speed, float health, float damage, Sound soundAttack, float width, float height) {
        super(stepsR, stepsL, passStateR, passStateL, shootingR, shootingL, position, speed, health, damage, soundAttack, width, height);

        resize = new Resize();

        bulletGenerator = new BulletGenerator(bulletRight, bulletLeft, new Vector2(0f, 0f), getDamage(), resize.calcWidth(15f), resize.calcWidth(14f), resize.calcHeight(5f));

        setCurrentAnimation(passStateR);
        setTempCurrentAnimation(passStateR);

        borders = new Borders();
    }

    public BulletGenerator getBulletGenerator() {
        return bulletGenerator;
    }

    /**
     * Метод отрисовывает персонажа
     *
     * @param batch - отрисовщик
     */
    @Override
    public void draw(SpriteBatch batch) {
        batch.draw(getCurrentAnimation().getFrame(), getBounds().x, getBounds().y, getWidth(), getHeight());
        for (int i = 0; i < bulletGenerator.getBullets().size; i++) {
            bulletGenerator.getBullets().get(i).draw(batch);
        }
    }

    /**
     * Метод обрабатывает анимации персонажа
     *
     * @param dt - интервал времени между текущим и последним кадром в секундах
     */
    @Override
    public void update(float dt) {
        if (getCurrentAnimation() != getTempCurrentAnimation()) {
            returnToStartAnimations();
            setTempCurrentAnimation(getCurrentAnimation());
        }

        setAnimation();

        createBullet();

        for (int i = 0; i < bulletGenerator.getBullets().size; i++) {
            bulletGenerator.getBullets().get(i).update(dt);
        }

        getCurrentAnimation().update(dt);

        perimeterRestrictions();
    }

    @Override
    public void dispose() {

    }

    /**
     * Метод устанвавливает анимацию шагов вправо
     */
    private void goRight() {
        setCurrentAnimation(getStepsR());
    }

    /**
     * Метод устанвавливает анимацию шагов влево
     */
    private void goLeft() {
        setCurrentAnimation(getStepsL());
    }

    /**
     * Метод устанвавливает правую пассивную анимацию
     */
    private void standRight() {
        setCurrentAnimation(getPassStateR());
    }

    /**
     * Метод устанвавливает левую пассивную анимацию
     */
    private void standLeft() {
        setCurrentAnimation(getPassStateL());
    }

    /**
     * Метод устанвавливает правую анимацию стрельбы
     */
    private void attackRight() {
        setCurrentAnimation(getAttackR());
    }

    /**
     * Метод устанвавливает левую анимацию стрельбы
     */
    private void attackLeft() {
        setCurrentAnimation(getAttackL());
    }

    /**
     * Метод устанавливает анимацию атак персонажа
     *
     * @return - true или false
     */
    public void attack(boolean isDownTouch) {
        if (isDownTouch) {
            //Анимация стрельбы вправо, если предыдущая анимация была: бегом вправо или стрельбой вправо, и была нажата кнопка стрельбы
            if ((!getCurrentAnimation().equals(getAttackR())) && ((getCurrentAnimation().equals(getStepsR())) || (getCurrentAnimation().equals(getPassStateR())))) {
                attackRight();
            }

            //Анимация стрельбы влево, если предыдущая анимация была: бегом влево или стрельбой влево, и была нажата кнопка стрельбы
            if ((!getCurrentAnimation().equals(getAttackL())) && ((getCurrentAnimation().equals(getStepsL())) || (getCurrentAnimation().equals(getPassStateL())))) {
                attackLeft();
            }
        }

        if (!isDownTouch) {
            //Если не нажата кнопка стрельбы и последняя анимация была стрельбой вправо, то устанавливается пассивное состояние вправо
            if (getCurrentAnimation().equals(getAttackR())) {
                standRight();
            }

            //Если не нажата кнопка стрельбы и последняя анимация была стрельбой влево, то устанавливается пассивное состояние влево
            if (getCurrentAnimation().equals(getAttackL())) {
                standLeft();
            }
        }
    }

    private void createBullet() {
        if (getCurrentAnimation().equals(getAttackR()) && getCurrentAnimation().getNumFrame() == 1 && !isAttackRight) {
            isAttackRight = true;
            getSoundAttack().play();
            bulletGenerator.setPosition(new Vector2(getPosition().x + resize.calcWidth(33f), getPosition().y + resize.calcHeight(12f)));
            bulletGenerator.createBullet(isAttackRight, isAttackLeft);
        }

        if (getCurrentAnimation().equals(getAttackL()) && getCurrentAnimation().getNumFrame() == 1 && !isAttackLeft) {
            isAttackLeft = true;
            getSoundAttack().play();
            bulletGenerator.setPosition(new Vector2(getPosition().x - resize.calcWidth(33f), getPosition().y + resize.calcHeight(12f)));
            bulletGenerator.createBullet(isAttackRight, isAttackLeft);
        }

        if ((getCurrentAnimation().equals(getAttackR()) && getCurrentAnimation().getNumFrame() != 1))
            isAttackRight = false;

        if ((getCurrentAnimation().equals(getAttackL()) && getCurrentAnimation().getNumFrame() != 1))
            isAttackLeft = false;
    }

    private void control() {
        getPosition().add(getDirection().x * getSpeed(), getDirection().y * getSpeed());

        getBounds().setPosition(new Vector2(getPosition().x - getWidth() / 2f, getPosition().y - getHeight() / 2f));
    }

    private void setAnimation() {
        if ((!getCurrentAnimation().equals(getPassStateR())) && (getCurrentAnimation().equals(getStepsR())) && (getDirection().x == 0 && getDirection().y == 0)) {
            standRight();
        }

        if ((!getCurrentAnimation().equals(getPassStateL())) && (getCurrentAnimation().equals(getStepsL())) && (getDirection().x == 0 && getDirection().y == 0)) {
            standLeft();
        }

        if ((!getCurrentAnimation().equals(getStepsR())) && (getDirection().x > 0)) {
            goRight();
        }

        if ((!getCurrentAnimation().equals(getStepsL())) && (getDirection().x < 0)) {
            goLeft();
        }
    }


    /**
     * Метод возвращает анимации в исходное состояние
     */
    private void returnToStartAnimations() {
        getStepsR().returnToStart();
        getStepsL().returnToStart();
        getPassStateR().returnToStart();
        getPassStateL().returnToStart();
        getAttackR().returnToStart();
        getAttackL().returnToStart();
    }

    private void perimeterRestrictions() {
        if (Borders.borderLeft.contains(getPosition().x - getWidth() / 2f, getPosition().y) && getDirection().x < 0) {
            getBounds().setPosition(getPosition().x - getWidth() / 2f, (getPosition().y - getHeight() / 2f));
            standLeft();

        } else if (Borders.borderDown.contains(getPosition().x, getPosition().y - getHeight() / 2f) && getDirection().y < 0) {
            getBounds().setPosition(getPosition().x - getWidth() / 2f, (getPosition().y - getHeight() / 2f));

            if (getCurrentAnimation().equals(getStepsR())) {
                standRight();
            } else {
                standLeft();
            }

        } else if (Borders.borderRight.contains(getPosition().x + getWidth() / 2f - 45, getPosition().y) && getDirection().x > 0) {
            getBounds().setPosition(getPosition().x - getWidth() / 2f, (getPosition().y - getHeight() / 2f));
            standRight();

        } else if ((Borders.borderUpR.contains(getPosition().x, getPosition().y + getHeight() / 2f) && getDirection().y > 0) || ((Borders.borderUpR.contains(getPosition().x + getWidth() / 2f, getPosition().y)) &&  getDirection().x > 0)) {
            getBounds().setPosition(getPosition().x - getWidth() / 2f, (getPosition().y - getHeight() / 2f));

            if (getCurrentAnimation().equals(getStepsR())) {
                standRight();
            } else {
                standLeft();
            }

        }else if ((Borders.borderUpL.contains(getPosition().x, getPosition().y + getHeight() / 2f) && getDirection().y > 0) || ((Borders.borderUpL.contains(getPosition().x - getWidth() / 2f, getPosition().y)) &&  getDirection().x < 0)) {
            getBounds().setPosition(getPosition().x - getWidth() / 2f, (getPosition().y - getHeight() / 2f));

            if (getCurrentAnimation().equals(getStepsR())) {
                standRight();
            } else {
                standLeft();
            }

        } else {
            control();
        }
    }

    /**
     * Метод отвечает за нанесение урона
     *
     * @param damage - урон
     */
    @Override
    public void hit(float damage) {
        setHealth(getHealth() - damage);
    }
}

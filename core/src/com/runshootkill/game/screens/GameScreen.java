package com.runshootkill.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.runshootkill.game.Main;
import com.runshootkill.game.constants.AppConstants;
import com.runshootkill.game.constants.LinksConstants;
import com.runshootkill.game.controllers.GameController;
import com.runshootkill.game.entities.MeleeNPC;
import com.runshootkill.game.entities.Player;
import com.runshootkill.game.animation.Animation;
import com.runshootkill.game.universalInterfaces.Button;
import com.runshootkill.game.universalInterfaces.Joystick;
import com.runshootkill.game.utilities.BulletsCollision;
import com.runshootkill.game.utilities.InfoPanel;
import com.runshootkill.game.utilities.Resize;

/**
 * @author Лукьянов В. А.
 * @author Лёвин В.А
 * @author Киреев Р.А
 */
public class GameScreen implements Screen {

    private OrthographicCamera camera;

    private Texture map;
    private Texture joystickCircle;
    private Texture joystickStick;
    private Texture btnShootTxtr;
    private Texture pStepsR;
    private Texture pStepsL;
    private Texture pPassR;
    private Texture pPassL;
    private Texture pShootR;
    private Texture pShootL;
    private Texture pBulletRight;
    private Texture pBulletLeft;
    private Texture melNpcStepsR;
    private Texture melNpcStepsL;
    private Texture melNpcPassR;
    private Texture melNpcPassL;
    private Texture melNpcAttackR;
    private Texture melNpcAttackL;
    private Texture infPanel;
    private Texture heart;
    private Texture hearts;
    private Texture ammo;
    private Texture bullet;

    private Sound soundFire;
    private Sound soundSword;

    private Player player;

    private MeleeNPC meleeNPC;

    private Button btnShoot;

    private Joystick joystick;

    private InfoPanel infoPanel;

    private Resize resize;

    private BulletsCollision bulletsCollision;

    public GameScreen(final Main main) {

        camera = new OrthographicCamera(Main.width, Main.height);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);

        resize = new Resize();

        textureInit();
        soundInit();
        controlInit();
        actorsInit();

        GameController gameController = new GameController(main, btnShoot, joystick, player);
        Gdx.input.setInputProcessor(gameController);

        infoPanel = new InfoPanel(player, infPanel, heart, hearts, ammo, bullet, new Vector2(resize.calcWidth(30), resize.calcHeight(AppConstants.APP_HEIGHT - 100)), resize.calcWidth(169), resize.calcHeight(75));
        bulletsCollision = new BulletsCollision(player, meleeNPC);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl20.glClearColor(1f, 1f, 1f, 1f);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        Main.batch.setProjectionMatrix(camera.combined);

        update(delta);

        Main.batch.begin();
        draw(Main.batch);
        Main.batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        map.dispose();
        joystickCircle.dispose();
        joystickStick.dispose();
        btnShootTxtr.dispose();
        pStepsR.dispose();
        pStepsL.dispose();
        pPassR.dispose();
        pPassL.dispose();
        pShootR.dispose();
        pShootL.dispose();
        pBulletRight.dispose();
        pBulletLeft.dispose();
        melNpcStepsR.dispose();
        melNpcStepsL.dispose();
        melNpcPassR.dispose();
        melNpcPassL.dispose();
        melNpcAttackR.dispose();
        melNpcAttackL.dispose();
        soundFire.dispose();
        soundSword.dispose();
        infPanel.dispose();
    }

    private void update(float dt) {
        player.update(dt);
        meleeNPC.update(dt);
        infoPanel.update(dt);
        bulletsCollision.update();
    }

    private void draw(SpriteBatch batch) {
        batch.draw(map, 0f, 0f, resize.calcWidth(AppConstants.APP_WIDTH), resize.calcHeight(AppConstants.APP_HEIGHT));
        player.draw(batch);
        meleeNPC.draw(batch);
        joystick.draw(batch);
        btnShoot.draw(batch);
        infoPanel.draw(batch);
    }

    private void textureInit() {
        map = new Texture(LinksConstants.MAP);

        joystickCircle = new Texture(LinksConstants.JOYSTICK_CIRCLE);
        joystickStick = new Texture(LinksConstants.JOYSTICK_STICK);

        btnShootTxtr = new Texture(LinksConstants.BTN_SHOOT);

        pBulletRight = new Texture(LinksConstants.P_BULLET_RIGHT);
        pBulletLeft = new Texture(LinksConstants.P_BULLET_LEFT);

        pStepsR = new Texture(LinksConstants.P_STEPS_RIGHT);
        pStepsL = new Texture(LinksConstants.P_STEPS_LEFT);
        pPassR = new Texture(LinksConstants.P_PASS_RIGHT);
        pPassL = new Texture(LinksConstants.P_PASS_LEFT);
        pShootR = new Texture(LinksConstants.P_SHOOT_RIGHT);
        pShootL = new Texture(LinksConstants.P_SHOOT_LEFT);

        melNpcStepsR = new Texture(LinksConstants.MEL_NPC_STEPS_RIGHT);
        melNpcStepsL = new Texture(LinksConstants.MEL_NPC_STEPS_LEFT);
        melNpcPassR = new Texture(LinksConstants.MEL_NPC_PASS_RIGHT);
        melNpcPassL = new Texture(LinksConstants.MEL_NPC_PASS_LEFT);
        melNpcAttackR = new Texture(LinksConstants.MEL_NPC_ATTACK_RIGHT);
        melNpcAttackL = new Texture(LinksConstants.MEL_NPC_ATTACK_LEFT);

        infPanel = new Texture(LinksConstants.INFO_PANEL);
        hearts = new Texture(LinksConstants.INFO_PANEL_HEARTS);
        heart = new Texture(LinksConstants.INFO_PANEL_HEART);
        ammo = new Texture(LinksConstants.INFO_PANEL_AMMO);
        bullet = new Texture(LinksConstants.INFO_PANEL_BULLET);
    }

    private void soundInit() {
        soundFire = Gdx.audio.newSound(Gdx.files.internal(LinksConstants.SOUND_FIRE));
        soundSword = Gdx.audio.newSound(Gdx.files.internal(LinksConstants.SOUND_SWORD));
    }

    private void actorsInit() {
        Animation pStepsRAnim = new Animation(pStepsR, 8, 1f);
        Animation pStepsLAnim = new Animation(pStepsL, 8, 1f);
        Animation pPassRAnim = new Animation(pPassR, 5, 1f);
        Animation pPassLAnim = new Animation(pPassL, 5, 1f);
        Animation pShootRAnim = new Animation(pShootR, 5, 0.8f);
        Animation pShootLAnim = new Animation(pShootL, 5, 0.8f);

        player = new Player(pStepsRAnim, pStepsLAnim, pPassRAnim, pPassLAnim, pShootRAnim, pShootLAnim, pBulletRight, pBulletLeft,
                new Vector2(resize.calcWidth(400f), resize.calcHeight(240f)), resize.calcWidth(2f), 100f, 5f, soundFire, resize.calcWidth(61f), resize.calcHeight(52f));

        Animation melNpcStepsRAnim = new Animation(melNpcStepsR, 4, 1f);
        Animation melNpcStepsLAnim = new Animation(melNpcStepsL, 4, 1f);
        Animation melNpcPassRAnim = new Animation(melNpcPassR, 4, 1f);
        Animation melNpcPassLAnim = new Animation(melNpcPassL, 4, 1f);
        Animation melNpcAttackRAnim = new Animation(melNpcAttackR, 3, 0.8f);
        Animation melNpcAttackLAnim = new Animation(melNpcAttackL, 3, 0.8f);

        meleeNPC = new MeleeNPC(player, melNpcStepsRAnim, melNpcStepsLAnim, melNpcPassRAnim, melNpcPassLAnim, melNpcAttackRAnim, melNpcAttackLAnim,
                new Vector2(700f, 240f), resize.calcWidth(1.2f), 100f, 10f, soundSword, resize.calcWidth(61f), resize.calcHeight(52f));
    }

    private void controlInit() {
        joystick = new Joystick(joystickCircle, joystickStick, new Vector2(resize.calcWidth(100f), resize.calcHeight(100f)), resize.calcHeight(188f));
        btnShoot = new Button(btnShootTxtr, new Vector2(resize.calcWidth(AppConstants.APP_WIDTH - 100f), resize.calcHeight(100f)), resize.calcWidth(156f), resize.calcHeight(156f));
    }
}
package com.runshootkill.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.runshootkill.game.Main;
import com.runshootkill.game.constants.AppConstants;
import com.runshootkill.game.constants.LinksConstants;
import com.runshootkill.game.controllers.MainMenuController;
import com.runshootkill.game.universalInterfaces.Button;
import com.runshootkill.game.utilities.Resize;

/**
 * @author Лукьянов В. А.
 * @author Лёвин В. А.
 */
public class MainMenuScreen implements Screen {
    private Texture bgMenu;
    private Texture gmName;
    private Texture btnStartUnpr;
    private Texture btnStartPr;
    private Texture btnExitUnpr;
    private Texture btnExitPr;

    private Button btnStart;
    private Button btnExit;

    private Music music;

    private Resize resize;

    public MainMenuScreen(Main main) {
        resize = new Resize();

        textureInit();

        createStartBtn();
        createExitBtn();

        music = Gdx.audio.newMusic(Gdx.files.internal(LinksConstants.BG_MUSIC));

        MainMenuController mainMenuController = new MainMenuController(main, btnStart, btnExit, music);
        Gdx.input.setInputProcessor(mainMenuController);

        music.play();
        music.setLooping(true);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Main.batch.begin();
        draw();
        Main.batch.end();
    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        bgMenu.dispose();
        gmName.dispose();
        btnStartPr.dispose();
        btnStartUnpr.dispose();
        btnExitPr.dispose();
        btnExitUnpr.dispose();
        music.dispose();
    }

    private void draw() {
        Main.batch.draw(bgMenu, 0f, 0f, Main.width, Main.height);
        Main.batch.draw(gmName, resize.calcWidth((float) AppConstants.APP_WIDTH / 2f - 600f / 2f), resize.calcWidth(((float) AppConstants.APP_HEIGHT - 70f) - resize.calcHeight(75f) / 2f),
                resize.calcWidth(600f), resize.calcHeight(75f));
        btnStart.draw(Main.batch);
        btnExit.draw(Main.batch);
    }

    private void textureInit() {
        bgMenu = new Texture(LinksConstants.BG_MENU);
        gmName = new Texture(LinksConstants.GM_NAME);
        btnStartUnpr = new Texture(LinksConstants.BTN_START_UNPR);
        btnStartPr = new Texture(LinksConstants.BTN_START_PR);
        btnExitUnpr = new Texture(LinksConstants.BTN_EXIT_UNPR);
        btnExitPr = new Texture(LinksConstants.BTN_EXIT_PR);
    }

    private void createStartBtn() {
        Vector2 position = new Vector2(resize.calcWidth((float) AppConstants.APP_WIDTH / 2f), resize.calcWidth(240));
        btnStart = new Button(btnStartUnpr, btnStartPr, position, resize.calcWidth(170f), resize.calcHeight(80f));
    }

    private void createExitBtn() {
        Vector2 position = new Vector2(resize.calcWidth((float) AppConstants.APP_WIDTH/ 2f), resize.calcHeight(120f));
        btnExit = new Button(btnExitUnpr, btnExitPr, position, resize.calcWidth(170f), resize.calcHeight(80f));
    }
}

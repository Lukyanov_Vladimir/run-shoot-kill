package com.runshootkill.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.runshootkill.game.Main;
import com.runshootkill.game.constants.LinksConstants;
import com.runshootkill.game.animation.Animation;

/**м
 * @author Лёвин В. А.
 */
public class PreviewScreen implements Screen {
    private final Main main;

    private Texture previewImg;

    private Animation previewAnim;

    public PreviewScreen(final  Main main) {
        this.main = main;

        previewImg = new Texture(LinksConstants.PREVIEW_IMG);

        previewAnim = new Animation(previewImg, 23, 2.5f);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl20.glClearColor(1f, 1f, 1f, 1f);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        previewAnim.update(delta);

        Main.batch.begin();
        Main.batch.draw(previewAnim.getFrame(), 0f, 0f, Main.width, Main.height);
        Main.batch.end();

        if (previewAnim.isEndAnimation()) {
            main.setScreen(new MainMenuScreen(main));
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        previewImg.dispose();
    }
}

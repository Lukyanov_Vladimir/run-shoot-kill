package com.runshootkill.game.universalInterfaces;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.runshootkill.game.abstractСlasses.GraphicsObj;

/**
 * @author Лукьянов В. А.
 */
public class Bullet extends GraphicsObj {
    private float speed;
    private float damage;

    public Bullet(Texture imgBullet, Vector2 position, Vector2 direction, float speed, float damage, float width, float height) {
        super(imgBullet, new Vector2(position), width, height);

        this.speed = speed;
        this.damage = damage;

        setDirection(new Vector2(direction));
        setBounds(new Rectangle(position.x - width / 2f, position.y - height / 2f, width, height));
    }

    public float getSpeed() {
        return speed;
    }

    public float getDamage() {
        return damage;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }

    @Override
    public void draw(SpriteBatch batch) {
        batch.draw(getImgObj(), getBounds().x, getBounds().y, getWidth(), getHeight());
    }

    @Override
    public void update(float dt) {
        getPosition().add(getDirection().x * speed, getDirection().y * speed);
        getBounds().setPosition(new Vector2(getPosition().x - getWidth() / 2f, getPosition().y - getHeight() / 2f));
    }

    @Override
    public void dispose() {

    }
}
package com.runshootkill.game.universalInterfaces;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.runshootkill.game.abstractСlasses.GraphicsObj;

/**
 * @author Лукьянов В. А.
 */
public class Button extends GraphicsObj {
    private Texture imgNotPressBtn;
    private Texture imgPressBtn;
    private Texture currentTexture;

    public Button(Texture imgButton, Vector2 position, float width, float height) {
        super(imgButton, position, width, height);

        setBounds(new Rectangle(position.x - width / 2f, position.y - height / 2f, width, height));

        currentTexture = imgButton;
    }

    public Button(Texture imgNotPressBtn, Texture imgPressBtn, Vector2 position, float width, float height) {
        this.imgNotPressBtn = imgNotPressBtn;
        this.imgPressBtn = imgPressBtn;

        setPosition(position);
        setWidth(width);
        setHeight(height);
        setBounds(new Rectangle(position.x - width / 2, position.y - height / 2, width, height));

        currentTexture = imgNotPressBtn;
    }

    public boolean isPressed(Vector2 touch, boolean isDownTouch) {
        boolean pressed = false;

        if (getBounds().contains(touch) && isDownTouch) {
            pressed = true;

            if (getImgObj() == null)
                currentTexture = imgPressBtn;
        }

        if (!isDownTouch || !getBounds().contains(touch)) {
            pressed = false;

            if (getImgObj() == null)
                currentTexture = imgNotPressBtn;
        }

        return pressed;
    }

    public void draw(SpriteBatch batch) {
        batch.draw(currentTexture, getBounds().x, getBounds().y, getWidth(), getHeight());
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void dispose() {

    }
}

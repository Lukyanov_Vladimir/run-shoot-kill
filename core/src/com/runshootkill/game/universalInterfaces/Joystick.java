package com.runshootkill.game.universalInterfaces;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.runshootkill.game.abstractСlasses.GraphicsObj;

/**
 * Класс создания джойстика
 *
 * @author Лукьянов В. А.
 */
public class Joystick extends GraphicsObj {
    private Texture circleImg;
    private Texture stickImg;

    private Circle circleBounds;
    private Circle stickBounds;

    private float radiusCircle;
    private float radiusStick;
    private float sizeCircle;
    private float sizeStick;
    private float size;

    private boolean wasPressing = false;

    /**
     * Конструктор класса Joystick
     *
     * @param circleImg - текстура окружности джойстика
     * @param stickImg  - текстура стика джойстика
     * @param position  - позиция
     * @param size      - размер
     */
    public Joystick(Texture circleImg, Texture stickImg, Vector2 position, float size) {
        this.circleImg = circleImg;
        this.stickImg = stickImg;
        this.size = size;

        setPosition(position);

        radiusCircle = size / 2f;
        radiusStick = radiusCircle / 2f;

        sizeCircle = size;
        sizeStick = size / 2f;

        circleBounds = new Circle(position, radiusCircle);
        stickBounds = new Circle(position, radiusStick);

        setDirection(new Vector2(0f, 0f));
    }

    public Circle getCircleBounds() {
        return circleBounds;
    }

    public Circle getStickBounds() {
        return stickBounds;
    }

    public float getRadiusCircle() {
        return radiusCircle;
    }

    public float getRadiusStick() {
        return radiusStick;
    }

    public float getSizeCircle() {
        return sizeCircle;
    }

    public float getSizeStick() {
        return sizeStick;
    }

    public float getSize() {
        return size;
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void dispose() {
    }

    /**
     * Метод отрисовывает джойстик
     *
     * @param batch - отрисовщик
     */
    public void draw(SpriteBatch batch) {
        batch.draw(circleImg, circleBounds.x - radiusCircle, circleBounds.y - radiusCircle, sizeCircle, sizeCircle);
        batch.draw(stickImg, stickBounds.x - radiusStick, stickBounds.y - radiusStick, sizeStick, sizeStick);
    }

    /**
     * Метод обрабатывает нажатие
     *
     * @param touch       - позиция по оси x и y
     * @param isDownTouch - нажатие
     */
    public void update(Vector2 touch, boolean isDownTouch) {
        if (isDownTouch)
            controlStick(touch);

        if (!isDownTouch)
            returnStick();
    }

    /**
     * Метод управляет стиком джойстика
     *
     * @param touch - координаты нажатия
     */
    private void controlStick(Vector2 touch) {
        if (circleBounds.contains(touch)) {
            wasPressing = true;
            stickBounds.setPosition(touch);
            identifyDirections();

        } else if (wasPressing) {
            float dx = touch.x - circleBounds.x;
            float dy = touch.y - circleBounds.y;

            float dist = (float) Math.sqrt(dx * dx + dy * dy);

            float rotationCoefficient = radiusCircle / dist;

            stickBounds.setPosition(new Vector2(dx * rotationCoefficient + circleBounds.x, dy * rotationCoefficient + circleBounds.y));
            identifyDirections();
        }
    }

    /**
     * Метод расчитывает направление стика
     */
    private void identifyDirections() {
        float dx = circleBounds.x - stickBounds.x;
        float dy = circleBounds.y - stickBounds.y;

        float dist = (float) Math.sqrt(dx * dx + dy * dy);

        if (dist != 0f)
            getDirection().set(-(dx / dist), -(dy / dist));
    }

    /**
     * Метод возвращает стик в исходные координаты
     */
    private void returnStick() {
        wasPressing = false;
        stickBounds.setPosition(circleBounds.x, circleBounds.y);
        getDirection().set(0f, 0f);
    }
}

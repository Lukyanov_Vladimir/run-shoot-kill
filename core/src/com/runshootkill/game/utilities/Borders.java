package com.runshootkill.game.utilities;

import com.badlogic.gdx.math.Rectangle;
import com.runshootkill.game.constants.AppConstants;

public class Borders {
    public static Rectangle borderLeft;
    public static Rectangle borderRight;
    public static Rectangle borderDown;
    public static Rectangle borderUpR;
    public static Rectangle borderUpL;

    public Borders() {
        Resize resize = new Resize();
        borderLeft = new Rectangle(resize.calcWidth(0f), resize.calcHeight(0), resize.calcWidth(25f), resize.calcHeight(AppConstants.APP_HEIGHT));
        borderDown = new Rectangle(resize.calcWidth(0f), resize.calcHeight(0f), resize.calcWidth(AppConstants.APP_WIDTH), resize.calcHeight(38f));
        borderUpL = new Rectangle(resize.calcWidth(0f), resize.calcHeight(AppConstants.APP_HEIGHT - 48f), resize.calcWidth(285f), resize.calcHeight(58f));
        borderUpR = new Rectangle(resize.calcWidth(AppConstants.APP_WIDTH-440), resize.calcHeight(AppConstants.APP_HEIGHT - 48f), resize.calcWidth(420f), resize.calcHeight(58f));
        borderRight = new Rectangle(resize.calcWidth(AppConstants.APP_WIDTH-27), resize.calcHeight(0), resize.calcWidth(25f), resize.calcHeight(AppConstants.APP_HEIGHT));
    }
}

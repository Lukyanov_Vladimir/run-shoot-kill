package com.runshootkill.game.utilities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.runshootkill.game.universalInterfaces.Bullet;

/**
 * @author Лукьянов В. А.
 */
public class BulletGenerator {
    private Array<Bullet> bullets;

    private Texture bulletRight;
    private Texture bulletLeft;

    private Vector2 position;

    private float damage;
    private float speed;
    private float width;
    private float height;

    public BulletGenerator(Texture bulletRight, Texture bulletLeft, Vector2 position, float damage, float speed, float width, float height) {
        this.bulletRight = bulletRight;
        this.bulletLeft = bulletLeft;
        this.position = position;
        this.damage = damage;
        this.speed = speed;
        this.width = width;
        this.height = height;

        bullets = new Array<>();
    }

    public Array<Bullet> getBullets() {
        return bullets;
    }

    public Vector2 getPosition() {
        return position;
    }

    public float getDamage() {
        return damage;
    }

    public float getSpeed() {
        return speed;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public void setBullets(Array<Bullet> bullets) {
        this.bullets = bullets;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public void createBullet(boolean isAttackRight, boolean isAttackLeft) {
        if (isAttackRight) {
            bullets.add(new Bullet(bulletRight, position, new Vector2(1f, 0f), speed, damage, width, height));
        }

        if (isAttackLeft) {
            bullets.add(new Bullet(bulletLeft, position, new Vector2(-1f, 0f), speed, damage, width, height));
        }
    }
}

package com.runshootkill.game.utilities;

import com.badlogic.gdx.utils.Array;
import com.runshootkill.game.Main;
import com.runshootkill.game.universalInterfaces.Bullet;

import java.util.Iterator;

/**
 * @author Лукьянов В. А.
 */
public class BulletRemoval {

    public void deleteBullets(Array<Bullet> bullets) {
        Iterator<Bullet> iterator = bullets.iterator();

        while (iterator.hasNext()) {
            Bullet bullet = iterator.next();

            if (bullet.getPosition().x > Main.width ||
                    bullet.getPosition().x < 0 ||
                    bullet.getPosition().y > Main.height ||
                    bullet.getPosition().y < 0)
                iterator.remove();
        }
    }
}

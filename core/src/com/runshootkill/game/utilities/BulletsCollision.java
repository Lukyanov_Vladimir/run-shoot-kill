package com.runshootkill.game.utilities;

import com.runshootkill.game.Main;
import com.runshootkill.game.entities.MeleeNPC;
import com.runshootkill.game.entities.Player;
import com.runshootkill.game.universalInterfaces.Bullet;

import java.util.Iterator;

public class BulletsCollision {
    private Player player;
    private MeleeNPC meleeNPC;

    public BulletsCollision(Player player, MeleeNPC meleeNPC) {
        this.player = player;
        this.meleeNPC = meleeNPC;
    }

    public void update() {
        Iterator<Bullet> iterator = player.getBulletGenerator().getBullets().iterator();

        while (iterator.hasNext()) {
            Bullet bullet = iterator.next();
            if (bullet.getBounds().overlaps(meleeNPC.getBounds())) {
                meleeNPC.hit(bullet.getDamage());
                iterator.remove();
            }

            if (Borders.borderLeft.contains(bullet.getPosition().x - bullet.getWidth() / 2f, bullet.getPosition().y) && bullet.getDirection().x < 0) {
                iterator.remove();
            } else if (Borders.borderDown.contains(bullet.getPosition().x, bullet.getPosition().y - bullet.getHeight() / 2f) && bullet.getDirection().y < 0) {

                iterator.remove();
            } else if (Borders.borderRight.contains(bullet.getPosition().x + bullet.getWidth() / 2f, bullet.getPosition().y) && bullet.getDirection().x > 0) {
                iterator.remove();
            } else if ((Borders.borderUpR.contains(bullet.getPosition().x, bullet.getPosition().y + bullet.getHeight() / 2f) && bullet.getDirection().y > 0) || ((Borders.borderUpR.contains(bullet.getPosition().x + bullet.getWidth() / 2f, bullet.getPosition().y)) && bullet.getDirection().x > 0)) {

                iterator.remove();
            } else if ((Borders.borderUpL.contains(bullet.getPosition().x, bullet.getPosition().y + bullet.getHeight() / 2f) && bullet.getDirection().y > 0) || ((Borders.borderUpL.contains(bullet.getPosition().x - bullet.getWidth() / 2f, bullet.getPosition().y)) && bullet.getDirection().x < 0)) {
                iterator.remove();
            }
        }
    }
}

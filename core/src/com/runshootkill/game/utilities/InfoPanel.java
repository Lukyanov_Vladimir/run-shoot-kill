package com.runshootkill.game.utilities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.runshootkill.game.abstractСlasses.GraphicsObj;
import com.runshootkill.game.entities.Player;

/**
 * @author Киреев Р.А
 */

public class InfoPanel extends GraphicsObj {
    private Texture infPanel;
    private Texture hearts;
    private Texture ammo;
    private Texture heart;
    private Texture bullet;

    private Player player;

    private float tempHealth;

    private float scaleHearts;
    private float tempSHW;

    private Resize resize;

    public InfoPanel(Player player, Texture infPanel, Texture hearts, Texture ammo, Texture heart, Texture bullet, Vector2 position, float width, float height) {
        setPosition(position);
        setWidth(width);
        setHeight(height);

        this.infPanel = infPanel;
        this.hearts = hearts;
        this.ammo = ammo;
        this.heart = heart;
        this.bullet = bullet;
        this.player = player;

        resize = new Resize();
        scaleHearts = resize.calcWidth(126);
        tempSHW = scaleHearts;

        tempHealth = player.getHealth();
    }

    @Override
    public void draw(SpriteBatch batch) {
        batch.draw(infPanel, getPosition().x, getPosition().y, getWidth(), getHeight());
        batch.draw(hearts, getPosition().x + resize.calcWidth(5f), getPosition().y + resize.calcHeight(50f), resize.calcWidth(20f), resize.calcHeight(21f));
        batch.draw(ammo, getPosition().x + resize.calcWidth(33f), getPosition().y + resize.calcHeight(52f), tempSHW, resize.calcHeight(13f));
        batch.draw(heart, getPosition().x + resize.calcWidth(33f), getPosition().y + resize.calcHeight(17f), resize.calcWidth(126f), resize.calcHeight(13f));
        batch.draw(bullet, getPosition().x + resize.calcWidth(5f), getPosition().y + resize.calcHeight(17f), resize.calcWidth(20f), resize.calcHeight(11f));
    }

    @Override
    public void update(float dt) {
        if (tempHealth > player.getHealth() && !(player.getHealth() < 0f)) {
            float d = (player.getHealth() * 100f) / tempHealth;
            float f = (tempSHW * d) / 100f;

            System.out.println(f);
            tempSHW = f;

            tempHealth = player.getHealth();
        }
    }

    @Override
    public void dispose() {

    }
}
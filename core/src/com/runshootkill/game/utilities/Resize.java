package com.runshootkill.game.utilities;

import com.runshootkill.game.Main;
import com.runshootkill.game.constants.AppConstants;

public class Resize {
    public float calcWidth(float num){
        return (float) Main.width / ((float) AppConstants.APP_WIDTH / num);
    }

    public float calcHeight(float num){
        return (float) Main.height / ((float) AppConstants.APP_HEIGHT / num);
    }
}
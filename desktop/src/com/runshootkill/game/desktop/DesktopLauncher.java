package com.runshootkill.game.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.runshootkill.game.Main;
import com.runshootkill.game.constants.AppConstants;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = AppConstants.APP_TITLE;
		config.width = AppConstants.APP_WIDTH;
		config.height = AppConstants.APP_HEIGHT;

		config.addIcon(AppConstants.APP_ICON, Files.FileType.Internal);

		new LwjglApplication(new Main(), config);
	}
}
